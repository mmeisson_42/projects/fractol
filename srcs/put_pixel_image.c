/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   put_pixel_image.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 17:46:35 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/25 10:09:52 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void			fractal_draw(t_datas *fractal)
{
	ft_bzero(fractal->addr, IMG_X * IMG_Y * sizeof(uint32_t));
	fractal->exec(fractal);
	mlx_put_image_to_window(fractal->mlx, fractal->win, fractal->img, 0, 0);
}

uint32_t		get_color(t_datas *fractal, int iter)
{
	uint8_t		rgb[3];
	int			set;

	set = fractal->color_set;
	if (iter < fractal->iter_max / 4)
	{
		rgb[set++ % 3] = iter * 255 / (fractal->iter_max / 4);
		rgb[set++ % 3] = 0;
		rgb[set % 3] = fractal->change + iter * 126 / (fractal->iter_max / 4);
	}
	else
	{
		rgb[set++ % 3] = iter * 255 / fractal->iter_max;
		rgb[set++ % 3] = 0;
		rgb[set % 3] = fractal->change + iter * 126 / fractal->iter_max;
	}
	return (rgb[2] + (rgb[1] << 8) + (rgb[0] << 16));
}

inline void		put_pixel_image(t_datas *fractal, int x, int y, uint32_t color)
{
	if (x > 0 && y > 0 && x < IMG_X && y < IMG_Y)
	{
		(fractal->addr)[x + y * IMG_X] = color;
	}
}
