/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mlx_hook.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 19:05:22 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/25 10:30:18 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"
#include <stdio.h>

int			key_get_color(t_datas *fractal, int keycode)
{
	if (keycode == ZERO)
		fractal->color_set = 0;
	else if (keycode == ONE)
		fractal->color_set = 1;
	else if (keycode == TWO)
		fractal->color_set = 2;
	else if (keycode == THREE)
		fractal->change = 0;
	else if (keycode == FOUR)
		fractal->change = 62;
	else if (keycode == FIVE)
		fractal->change = 124;
	else
		return (0);
	return (1);
}

int			key_hook(int keycode, t_datas *fractal)
{
	if (keycode == ESCAPE)
		exit(0);
	else if (keycode == S)
		fractal->motion = !fractal->motion;
	else if (keycode == SPACE)
		fractal->init(fractal);
	else if (keycode == PLUS || keycode == KPLUS)
		fractal->iter_max += 10;
	else if (keycode == MINUS || keycode == KMINUS)
	{
		if (fractal->iter_max > 10)
			fractal->iter_max -= 10;
	}
	else if (fractal_move(fractal, keycode) == 1)
		;
	else if (key_get_color(fractal, keycode) == 1)
		;
	else
		return (1);
	fractal_draw(fractal);
	return (0);
}

int			mouse_hook(int button, int x, int y, t_datas *fractal)
{
	t_coords	size;

	if (button == RIGHT_CLICK || button == WH_RIGHT || button == WH_UP)
	{
		size.x = (fractal->x2 - fractal->x1) / 2.0;
		size.y = (fractal->y2 - fractal->y1) / 2.0;
		fractal->x1 += (size.x * (long double)x / (long double)IMG_X);
		fractal->x2 = fractal->x1 + size.x;
		fractal->y1 += (size.y * (long double)y / (long double)IMG_Y);
		fractal->y2 = fractal->y1 + size.y;
		fractal->step /= 2.0;
	}
	else if (button == LEFT_CLICK || button == WH_LEFT || button == WH_DOWN)
	{
		size.x = (fractal->x2 - fractal->x1);
		size.y = (fractal->y2 - fractal->y1);
		fractal->x1 -= (size.x / 2.0);
		fractal->x2 = fractal->x1 + (size.x * 2.0);
		fractal->y1 -= (size.y / 2.0);
		fractal->y2 = fractal->y1 + (size.y * 2.0);
		fractal->step *= 2.0;
	}
	fractal_draw(fractal);
	return (0);
}

int			motion_hook(int x, int y, t_datas *fractal)
{
	if (fractal->motion == true)
	{
		if (x >= 0 && y >= 0 && x < IMG_X && y < IMG_Y)
		{
			fractal->c.r = ((long double)x / (long double)IMG_X * 4 - 2) / 2.5;
			fractal->c.i = ((long double)y / (long double)IMG_Y * 4 - 2) / 2.5;
			fractal_draw(fractal);
		}
	}
	return (0);
}
