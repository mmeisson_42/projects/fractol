/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractal_move.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/22 16:06:33 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/22 17:08:14 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int		fractal_move(t_datas *fractal, int keycode)
{
	if (keycode == LEFT)
	{
		fractal->x1 += fractal->step;
		fractal->x2 += fractal->step;
	}
	else if (keycode == RIGHT)
	{
		fractal->x1 -= fractal->step;
		fractal->x2 -= fractal->step;
	}
	else if (keycode == TOP)
	{
		fractal->y1 += fractal->step;
		fractal->y2 += fractal->step;
	}
	else if (keycode == BOT)
	{
		fractal->y1 -= fractal->step;
		fractal->y2 -= fractal->step;
	}
	else
		return (0);
	return (1);
}
