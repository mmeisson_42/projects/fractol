/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   burningship.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 18:55:03 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/24 16:13:23 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

#define ABS(x) ((x < 0) ? -x : x)

void			burningship_init(t_datas *fractal)
{
	fractal->change = 0;
	fractal->x1 = -2.0;
	fractal->x2 = 1.4;
	fractal->y1 = -2.2;
	fractal->y2 = 1.1;
	fractal->color_set = 0;
	fractal->step = (fractal->x2 - fractal->x1) / 10.0;
	fractal->iter_max = 50;
}

static void		burningship_iter(t_datas *fractal, int x, int y, t_coords zoom)
{
	t_complex		c;
	t_complex		z;
	long double		tmp;
	int				i;

	c.r = (long double)x / zoom.x + fractal->x1;
	c.i = (long double)y / zoom.y + fractal->y1;
	z.r = 0;
	z.i = 0;
	i = 0;
	while (z.r * z.r + z.i * z.i < 4 && i < fractal->iter_max)
	{
		tmp = z.i;
		z.i = (long double)(z.r * z.i + z.r * z.i + c.i);
		z.i = ABS(z.i);
		z.r = (long double)(z.r * z.r - tmp * tmp + c.r);
		z.r = ABS(z.r);
		i++;
	}
	if (i < fractal->iter_max)
		put_pixel_image(fractal, x, y, get_color(fractal, i));
}

void			burningship(t_datas *fractal)
{
	t_coords		zoom;
	int				x;
	int				y;

	x = 0;
	zoom.x = IMG_X / (fractal->x2 - fractal->x1);
	zoom.y = IMG_Y / (fractal->y2 - fractal->y1);
	while (x < IMG_X)
	{
		y = 0;
		while (y < IMG_Y)
		{
			burningship_iter(fractal, x, y, zoom);
			y++;
		}
		x++;
	}
}
