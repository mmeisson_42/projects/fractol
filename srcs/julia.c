/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   julia.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 18:55:26 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/25 10:24:47 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void			julia_init(t_datas *fractal)
{
	fractal->change = 0;
	fractal->x1 = -1.0;
	fractal->x2 = 1.0;
	fractal->y1 = -1.2;
	fractal->y2 = 1.2;
	fractal->color_set = 0;
	fractal->step = (fractal->x2 - fractal->x1) / 10.0;
	fractal->c.r = 0.3044;
	fractal->c.i = -0.0075;
	fractal->iter_max = 50;
	fractal->motion = true;
}

static void		julia_iter(t_datas *fractal, int x, int y, t_coords zoom)
{
	t_complex		z;
	long double		tmp;
	int				i;

	z.r = (long double)x / zoom.x + fractal->x1;
	z.i = (long double)y / zoom.y + fractal->y1;
	i = 0;
	while (z.r * z.r + z.i * z.i < 4 && i < fractal->iter_max)
	{
		tmp = z.r;
		z.r = z.r * z.r - z.i * z.i + fractal->c.r;
		z.i = 2 * z.i * tmp + fractal->c.i;
		i++;
	}
	if (i < fractal->iter_max)
		put_pixel_image(fractal, x, y, get_color(fractal, i));
}

void			julia(t_datas *fractal)
{
	t_coords		zoom;
	int				x;
	int				y;

	x = 0;
	zoom.x = IMG_X / (fractal->x2 - fractal->x1);
	zoom.y = IMG_Y / (fractal->y2 - fractal->y1);
	while (x < IMG_X)
	{
		y = 0;
		while (y < IMG_Y)
		{
			julia_iter(fractal, x, y, zoom);
			y++;
		}
		x++;
	}
}
