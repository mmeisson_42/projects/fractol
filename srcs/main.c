/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 17:05:14 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/28 13:45:52 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

void		fractal_init(int argc, const char *argv[], t_datas *fractal)
{
	const t_frac_assoc		tab[] = {
		{ "mandelbrot", mandelbrot, mandelbrot_init },
		{ "julia", julia, julia_init },
		{ "burningship", burningship, burningship_init },
	};
	int						i;

	i = 0;
	if (argc == 2)
	{
		while (i < 3)
		{
			if (ft_strequ(tab[i].name, argv[1]))
			{
				fractal->exec = tab[i].fractal;
				fractal->init = tab[i].fractal_init;
				break ;
			}
			i++;
		}
	}
	if (i == 3 || argc != 2)
		exit(ft_dprintf(2, "Error: Usage: ./fractol [mandelbrot - julia -%s\n",
					" burningship]"));
}

int			main(int ac, const char *av[])
{
	t_datas		fractal;

	fractal_init(ac, av, &fractal);
	if (!(fractal.mlx = mlx_init()))
		exit(ft_dprintf(2, "Error while initialising mlx\n"));
	if (!(fractal.win = mlx_new_window(fractal.mlx, IMG_X, IMG_Y, "Fract'ol")))
		exit(ft_dprintf(2, "Error while initialising mlx\n"));
	if (!(fractal.img = mlx_new_image(fractal.mlx, IMG_X, IMG_Y)))
		exit(ft_dprintf(2, "Error while initialising mlx\n"));
	fractal.addr = (uint32_t *)mlx_get_data_addr(fractal.img, &ac, &ac, &ac);
	fractal.init(&fractal);
	fractal_draw(&fractal);
	mlx_key_hook(fractal.win, &key_hook, &fractal);
	mlx_mouse_hook(fractal.win, &mouse_hook, &fractal);
	if (fractal.exec == julia)
		mlx_hook(fractal.win, MOUSE_HOOK, MOUSE_MASK, &motion_hook, &fractal);
	mlx_loop(fractal.mlx);
	return (0);
}
