/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/21 17:04:07 by mmeisson          #+#    #+#             */
/*   Updated: 2016/08/25 07:56:40 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <mlx.h>
# include <math.h>
# include <stdbool.h>
# include <stdint.h>
# include "libft.h"
# include "ft_stdio.h"

# define IMG_X 720
# define IMG_Y 640

# define MOUSE_MASK (1L<<6)
# define MOUSE_HOOK 6

typedef struct		s_complex
{
	long double		r;
	long double		i;
}					t_complex;

typedef struct		s_coords
{
	long double		x;
	long double		y;
}					t_coords;

typedef enum		e_mouse
{
	RIGHT_CLICK = 1,
	LEFT_CLICK = 2,
	MID_CLICK = 3,
	WH_UP = 4,
	WH_DOWN = 5,
	WH_RIGHT = 6,
	WH_LEFT = 7,
}					t_mouse;

typedef enum		e_keyboard
{
	PLUS = 24,
	MINUS = 27,
	KPLUS = 69,
	KMINUS = 78,
	SPACE = 49,
	LEFT = 123,
	RIGHT = 124,
	BOT = 125,
	TOP = 126,
	ESCAPE = 53,
	ZERO = 29,
	ONE = 18,
	TWO = 19,
	THREE = 20,
	FOUR = 21,
	FIVE = 23,
	SIX = 22,
	SEVEN = 26,
	EIGHT = 28,
	NINE = 25,
	S = 1,
}					t_keyboard;

typedef struct		s_datas
{
	void			*mlx;
	void			*win;
	void			*img;
	uint32_t		*addr;
	void			(*exec)(struct s_datas*);
	void			(*init)(struct s_datas*);
	int				iter_max;
	int				color_set;
	int				change;
	bool			motion;
	t_complex		c;
	long double		x1;
	long double		x2;
	long double		y1;
	long double		y2;
	long double		step;
}					t_datas;

typedef struct		s_frac_assoc
{
	const char	*name;
	void		(*fractal)(t_datas*);
	void		(*fractal_init)(t_datas*);
}					t_frac_assoc;

/*
**	put_pixel_image.c
*/
uint32_t			get_color(t_datas *fractal, int iter);
void				fractal_draw(t_datas *fractal);
void				put_pixel_image(t_datas *fractal, int x, int y,
		uint32_t color);

/*
**	mandelbrot.c
*/
void				mandelbrot_init(t_datas *fractal);
void				mandelbrot(t_datas *fractal);

/*
**	julia.c
*/
void				julia_init(t_datas *fractal);
void				julia(t_datas *fractal);

/*
**	mandelbrot.c
*/
void				burningship_init(t_datas *fractal);
void				burningship(t_datas *fractal);

/*
**	mlx_hook.c
*/
int					key_hook(int keycode, t_datas *param);
int					mouse_hook(int button, int x, int y, t_datas *fractal);
int					motion_hook(int x, int y, t_datas *fractal);

/*
**	fractal_move.c
*/
int					fractal_move(t_datas *fractal, int keycode);

#endif
