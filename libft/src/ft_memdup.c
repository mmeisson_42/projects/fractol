/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mmeisson <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/28 01:50:09 by mmeisson          #+#    #+#             */
/*   Updated: 2016/04/28 01:52:51 by mmeisson         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memdup(void *ptr, size_t size)
{
	unsigned char	*new;
	unsigned char	*mem;

	if (!(new = malloc(size)))
		return (NULL);
	mem = (unsigned char*)ptr;
	while (size--)
		new[size] = mem[size];
	return ((void*)new);
}
