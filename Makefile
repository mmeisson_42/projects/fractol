# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mmeisson <mmeisson@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/09/14 21:39:23 by mmeisson          #+#    #+#              #
#    Updated: 2016/08/24 14:50:37 by mmeisson         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = clang

NAME = fractol

SRC_PATH = ./srcs/
SRC_NAME = main.c put_pixel_image.c mandelbrot.c julia.c mlx_hook.c\
		   fractal_move.c burningship.c

OBJ_PATH = ./.obj/
OBJ_NAME = $(SRC_NAME:.c=.o)

INC_PATH = ./incs/ ./libft/includes/ ./printf/inc/

LIB_PATHS = ./libft/ ./printf/

CFLAGS = -Wall -Werror -Wextra -g -Ofast

SRC = $(addprefix $(SRC_PATH),$(SRC_NAME))
OBJ = $(addprefix $(OBJ_PATH),$(OBJ_NAME))
LIB = $(addprefix -L,$(LIB_PATHS))
INC = $(addprefix -I,$(INC_PATH))

LIB_NAMES = -lft -lmlx -lm -lftprintf
LDFLAGS = $(LIB) $(LIB_NAMES) -framework OpenGL -framework AppKit

all: $(NAME)

$(NAME): $(OBJ)
	$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS);)
	$(CC) $^ -o $@ $(LDFLAGS)

$(OBJ_PATH)%.o: $(SRC_PATH)%.c
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CFLAGS) $(INC) -o $@ -c $<

clean:
	$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS) clean;)
	@rm -rf $(OBJ_PATH)

fclean:
	$(foreach PATHS, $(LIB_PATHS), make -j8 -C $(PATHS) fclean;)
	@rm -rf $(OBJ_PATH)
	@rm -f $(NAME)

sdl:
	make -j8 -C ./fractol_SDL
	ln -s ./fractol_SDL/fractol fractol


re: fclean all
